# Release Dates file generation



## Summary

Starting in `FY24-Q4`, GitLab will be moving our release cycle from the 22nd of the month to the 3rd Thursday of each month.  Details are discussed in https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/124310. 

To make it easy for everyone to keep track of what the cutoff dates will be in the future, this project is intended to call the milestone API and extract out the end date and the associated milestone to place it into a JSON file that will be used as the SSOT for future reference by the organization and in our handbook.
