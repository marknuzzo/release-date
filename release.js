#!/usr/bin/env node
/* eslint-disable no-console */

"use strict";
// Constants and variables.
const axios = require("axios");
const config = require("./config.json");
let gitlabToken = process.env.GITLAB_RD_TOKEN;
const fileName = config.filename;
const getMilestonesURL = config.getMilestonesURL;
const postMilestonesURL = config.postMilestonesURL;
const configHeader = {
  headers:{
    "PRIVATE-TOKEN": gitlabToken
  }
};
var result = [];
var excArray = [];
excArray = config.milestoneExc;

const getDateMilestone = async () => {
  try {
    const response = await axios
      .get(
        getMilestonesURL,
        configHeader
      )
      .catch(function (error) {
        if (error) console.log("Axios error: " + error);
      });
      var data = response.data;

      for (var i = 0; i < data.length; i++) {
          // Add as long as it's not in the exception array.
          if((excArray.includes(data[i].title)) == false) {
            result.push({
                          EndDate: data[i].due_date, 
                          Milestone: data[i].title
            });
          }
      }

    const postResponse = await axios(
        {
          method: 'post',
          url: postMilestonesURL,
          headers: {
            "PRIVATE-TOKEN": gitlabToken,
            "Content-Type": "application/json"
          },
          data: {
            branch: 'main', 
            commit_message: 'Releases updated',
            actions: [
                {
                  'action': 'update',
                  'file_path': fileName,
                  'content': JSON.stringify(result)
                }
            ]
          }
        }
      )
      .catch(function (error) {
        if (error) console.log("Axios error: " + error);
      });
      
  } catch (err) {
    console.log(err);
  } 
};

getDateMilestone();
